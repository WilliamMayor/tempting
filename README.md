# tempting

Build HTML using templates written in normal Python code. Easily create reusable, modular, extendable components, and have the full power of Python at your fingertips. Build forms and interact with HTML inputs too.
