from tempting.tags import Placeholder as TPlaceholder
from tempting.errors import TemptingError


Omitted = object()


def default_process(*values):
    yield from values


class Placeholder(TPlaceholder):
    element = None

    def __init__(self, name, default=Omitted, cumulative=False, process=None):
        self.name = name
        self.default = default
        self.cumulative = cumulative
        self.values = []
        self.process = process or default_process
        super().__init__()

    @property
    def has_default(self):
        return self.default is not Omitted

    def add_value(self, value):
        if self.cumulative:
            self.values.append(value)
        else:
            self.values = [value]

    def replace(self):
        values = list(self.process(*self.values))
        if not values and self.default is not Omitted:
            values = [self.default]
        if not values:
            raise TemptingError(f'Cannot render placeholders, missing value for {self.name}')
        if self.parent is not None:
            self.replace_as_element(*values)
        if self.element is not None:
            self.replace_as_attribute(*values)

    def replace_as_element(self, *values):
        index = list(self.parent).index(self)
        self.parent.remove(self)
        for i, v in enumerate(values):
            if v is None:
                continue
            try:
                self.parent.insert(index + i, v)
                v.parent = self.parent
            except TypeError:
                self.parent.add_text(str(v), index=max(0, index + i - 1))

    def replace_as_attribute(self, value, *ignored):
        for k, v in self.element.items():
            if v is self:
                self.element.set(k, value)

    def render(self, **kwargs):
        raise TemptingError(f'Cannot render placeholders, missing value for {self.name}')

    def __str__(self):
        raise TemptingError(f'Cannot render placeholders, missing value for {self.name}')

    @property
    def text(self):
        """Overwrite Element.text so we can error out if we try to render a placeholder"""
        raise TemptingError(f'Cannot render placeholders, missing value for {self.name}')
