

class TemptingError(Exception):
    pass


class ValidationError(TemptingError):
    pass
