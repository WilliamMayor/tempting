import sys
from types import ModuleType
from xml.etree import ElementTree


def extended_init_factory(Parent, *args, **kwargs):

    def init(self, *a, **kw):
        kw = dict(kwargs, **kw)
        Parent.__init__(self, *args, *a, **kw)

    return init


class Tag(ElementTree.Element):
    _name = 'tag'
    parent = None

    @staticmethod
    def class_factory(name):
        return type(name, (Tag, ), {'_name': name.lower()})

    @classmethod
    def extend(cls, *args, **kwargs):
        name = f'{cls.__name__}Extended'
        base = (cls, )
        body = {
            '__init__': extended_init_factory(cls, *args, **kwargs)
        }
        return type(name, base, body)

    def __init__(self, *args, **kwargs):
        super().__init__(self._name)
        self.add_children(*args)
        self.set_attributes(**kwargs)
        self.add_placeholder_values(**kwargs)

    def add_children(self, *args):
        for a in args:
            try:
                self.append(a)
                a.parent = self
            except TypeError:
                self.add_text(str(a))

    def add_text(self, text, index=-1):
        if len(self):
            self[index].tail = (self[index].tail or '') + text
        else:
            self.text = (self.text or '') + text

    def set_attributes(self, **kwargs):
        for k, v in kwargs.items():
            if k.endswith('_'):
                k = k[:-1]
            k = k.replace('_', '-')
            try:
                if v._name == 'placeholder':
                    v.element = self
            except AttributeError:
                # Not a placeholder attribute
                pass
            self.set(k, v)

    def add_placeholder_values(self, **kwargs):
        for p in self.iter('placeholder'):
            if p.name in kwargs:
                p.add_value(kwargs[p.name])
                self.attrib.pop(p.name, None)
        for e in self.iter():
            for k, v in list(e.items()):
                try:
                    if v.name in kwargs:
                        v.add_value(kwargs[v.name])
                        self.attrib.pop(v.name, None)
                except AttributeError:
                    # Not a Placeholder attribute
                    pass

    def replace_placeholders(self):
        for p in self.iter('placeholder'):
            p.replace()
        for e in self.iter():
            for k, v in list(e.items()):
                try:
                    if v._name == 'placeholder':
                        v.replace()
                except AttributeError:
                    # Not a Placeholder attribute
                    continue

    def render(self, **kwargs):
        self.add_placeholder_values(**kwargs)
        self.replace_placeholders()
        return ElementTree.tostring(self, encoding='unicode', method='html')


class TagsModule(ModuleType):
    # Adapted from SelfWrapper from the sh library
    # https://github.com/amoffat/sh

    def __init__(self, self_module):
        for attr in ["__builtins__", "__doc__", "__file__", "__name__", "__package__"]:
            setattr(self, attr, getattr(self_module, attr, None))

    def __getattr__(self, name):
        if hasattr(self_module, name):
            return getattr(self_module, name)
        return Tag.class_factory(name)


self_module = sys.modules[__name__]
sys.modules[__name__] = TagsModule(self_module)
