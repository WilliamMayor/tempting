from tempting.tags import Form as TForm, Button as TButton, Input as TInput, Select as TSelect, Option as TOption, Textarea as TTextarea


class Form(TForm):
    pass


class Button(TButton):
    pass


class Input(TInput):
    pass


class Select(TSelect):
    pass


class Option(TOption):
    pass


class Textarea(TTextarea):
    pass
