from tempting.tags import Html as THtml


class Html(THtml):

    def render(self, **kwargs):
        rendered = super().render(**kwargs)
        return f'<!doctype html>{rendered}'
