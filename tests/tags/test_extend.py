from tempting.tags import Div


def test_can_extend():
    MyDiv = Div.extend(
        Div('It Works!'),
        id='123'
    )
    d = MyDiv()
    assert d.render() == '<div id="123"><div>It Works!</div></div>'


def test_can_add_elements():
    MyDiv = Div.extend(
        Div('It Works!'),
        id='123'
    )
    d = MyDiv(Div('It really works!'))
    assert d.render() == '<div id="123"><div>It Works!</div><div>It really works!</div></div>'


def test_extend_extended():
    HelloDiv = Div.extend(Div('Hello '))
    WorldDiv = HelloDiv.extend(Div('World!'))

    d = WorldDiv()
    assert d.render() == '<div><div>Hello </div><div>World!</div></div>'
