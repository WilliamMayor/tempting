from tempting.tags import Div, Br


def test_empty_tag():
    t = Div()
    assert t.render() == '<div></div>'


def test_void_tag():
    t = Br()
    assert t.render() == '<br>'


def test_tag_with_attribute():
    t = Div(id='id')
    assert t.render() == '<div id="id"></div>'


def test_tag_with_hyphenated_attribute():
    t = Div(data_id='id')
    assert t.render() == '<div data-id="id"></div>'


def test_tag_with_python_keyword_attribute():
    t = Div(class_='class')
    assert t.render() == '<div class="class"></div>'


def test_tag_with_children():
    t = Div(Div())
    assert t.render() == '<div><div></div></div>'


def test_tag_with_text():
    t = Div('Hello World!')
    assert t.render() == '<div>Hello World!</div>'


def test_tag_with_multiple_text():
    t = Div('Hello', ' World!')
    assert t.render() == '<div>Hello World!</div>'


def test_non_ascii_attribute():
    t = Div(and_='&')
    assert t.render() == '<div and="&amp;"></div>'


def test_non_ascii_text():
    t = Div('<')
    assert t.render() == '<div>&lt;</div>'
