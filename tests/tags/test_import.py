from tempting.tags import Tag, Something


def test_import_hardcoded():
    """Tag is hardcoded so we don't want to dynamically create it"""
    t = Tag()
    assert t.tag == 'tag'
    assert t.render() == '<tag></tag>'


def test_import_dynamic():
    """This tag is dynamically created on import, it should function like a tag"""
    s = Something()
    assert s.tag == 'something'
    assert s.render() == '<something></something>'
