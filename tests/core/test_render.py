from tempting.tags import Head, Body
from tempting.core import Html


def test_html_tag():
    t = Html(Head(), Body())
    assert t.render() == '<!doctype html><html><head></head><body></body></html>'
