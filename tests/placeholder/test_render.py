from tempting.tags import Div, Span, Ul, Li
from tempting.placeholder import Placeholder


def test_fill_element_placeholder_with_text():
    d = Div(Placeholder('contents'))
    assert d.render(contents='Hello World!') == '<div>Hello World!</div>'


def test_fill_element_placeholder_with_element():
    d = Div(Placeholder('contents'))
    assert d.render(contents=Div('Hello World!')) == '<div><div>Hello World!</div></div>'


def test_placeholder_with_default():
    d = Div(Placeholder('contents', default='Hello World!'))
    assert d.render() == '<div>Hello World!</div>'


def test_placeholder_override_default():
    d = Div(Placeholder('contents', default='Hello World!'))
    assert d.render(contents='Goodbye!') == '<div>Goodbye!</div>'


def test_placeholder_default_none():
    d = Div(Placeholder('contents', default=None))
    assert d.render() == '<div></div>'


def test_fill_nested_element_placeholder():
    d = Div(
        Div(Placeholder('contents'))
    )
    assert d.render(contents='Hello World!') == '<div><div>Hello World!</div></div>'


def test_fill_attribute_placeholder():
    d = Div(id=Placeholder('div_id'))
    assert d.render(div_id='123') == '<div id="123"></div>'


def test_fill_nested_attribute_placeholder():
    d = Div(
        Div(id=Placeholder('div_id'))
    )
    assert d.render(div_id='123') == '<div><div id="123"></div></div>'


def test_fill_repeated_placeholder():
    d = Div('My ID is ', Placeholder('div_id'), id=Placeholder('div_id'))
    assert d.render(div_id='123') == '<div id="123">My ID is 123</div>'


def test_extend_with_placeholder():
    MyDiv = Div.extend(
        class_=Placeholder('cls')
    )
    d = MyDiv('Hello World!')
    assert d.render(cls='foo') == '<div class="foo">Hello World!</div>'


def test_fill_placeholder_in_extended_init():
    MyDiv = Div.extend(
        class_=Placeholder('cls')
    )
    d = MyDiv('Hello World!', cls='foo')
    assert d.render() == '<div class="foo">Hello World!</div>'


def test_fill_placeholder_in_nested_extended_init():
    Greeter = Div.extend(
        'Hello, ',
        Span(Placeholder('name'), class_='name'),
        class_='greeter'
    )
    g = Greeter(name='George')
    assert g.render() == '<div class="greeter">Hello, <span class="name">George</span></div>'


def truthy_to_invalid(*replacements):
    if len(replacements):
        yield 'is-invalid'


def test_custom_process():
    d = Div('Hello World!', class_=Placeholder('errors', process=truthy_to_invalid))
    assert d.render(errors=[1, 2, 3]) == '<div class="is-invalid">Hello World!</div>'


def test_cumulative_text():
    Greeter = Div.extend(
        'Hello, ',
        Span(Placeholder('name', cumulative=True), class_='name'),
        class_='greeter'
    )
    g = Greeter(name='George')
    assert g.render(name=' and Harry') == '<div class="greeter">Hello, <span class="name">George and Harry</span></div>'


def test_cumulative_elements():
    Breadcrumbs = Ul.extend(Li('Home'), Placeholder('crumbs', cumulative=True))
    b = Breadcrumbs(crumbs=Li('Page'))
    assert b.render(crumbs=Li('Sub-Page')) == '<ul><li>Home</li><li>Page</li><li>Sub-Page</li></ul>'
