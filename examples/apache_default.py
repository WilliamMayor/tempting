"""
Let's create the apache2 default page! Should be easy!
"""
from tempting.tags import Head, Body, H1
from tempting.core import Html

page = Html(
    Head(),
    Body(
        H1('It works!')))

print(page.render())
