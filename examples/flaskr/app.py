from flask import Flask, request, session, abort, flash, redirect, url_for

from flaskr.templates import ShowEntriesPage, LoginPage, AddEntryForm

app = Flask(__name__)

app.config.update({
    'USERNAME': 'admin',
    'PASSWORD': 'hunter2',
    'SECRET_KEY': 'not a secret'})

ENTRIES = [
    {'title': '<Hello>', 'text': '<strong>HTML</strong> allowed here'}
]


@app.route('/')
def show_entries():
    return ShowEntriesPage(ENTRIES).render()


@app.route('/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    form = AddEntryForm(request.form)
    if form.validate():
        e = {
            'title': form.title.value,
            'text': form.text.contents
        }
        ENTRIES.append(e)
        flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    page = LoginPage(request.form)
    if request.method == 'POST' and page.form.validate():
        session['logged_in'] = True
        flash('You were logged in')
        return redirect(url_for('show_entries'))
    return page.render()


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))


if __name__ == '__main__':
    app.run(debug=True)
