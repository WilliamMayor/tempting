from flask import url_for, session, get_flashed_messages, current_app
from tempting.core import Html, Head, Link, Title, Text, Body, Div, H1, Ul, A, Li, H2, RawText, Em, Form, Input, Label, P, Textarea


class Flash(Div):

    def __init__(self, message):
        super().__init__(Text(message), class_='flash')


class Page(Html):

    def __init__(self, *content):
        head = Head(
            Link(
                rel='stylesheet',
                type='text/css',
                href=url_for('static', filename='style.css')),
            Title('Flaskr'))
        if not session.get('logged_in'):
            link = A(Text('log in'), href=url_for('login'))
        else:
            link = A(Text('log out'), href=url_for('logout'))
        body = Body(
            Div(
                H1(Text('Flaskr')),
                Div(
                    link,
                    *(Flash(m) for m in get_flashed_messages()),
                    class_='metanav'),
                *content,
                class_='page'))
        super().__init__(head, body)


class AddEntryForm(Form):

    def __init__(self, html_data):
        self.title = Input(type='text', name='title', size=30, html_value=html_data.get('title'))
        self.text = Textarea('', name='text', rows=5, cols=40, html_contents=html_data.get('text'))
        super().__init__(
            Field(self.title, help_text='The title of your new entry'),
            Field(self.text, help_text='The title of your new entry'),
            Input(type='submit', value='Share'),
            action=url_for('add_entry'), method='POST', class_='add-entry')

    def validate(self):
        return True


class ShowEntriesPage(Page):

    def __init__(self, entries):
        entry_list = Ul(class_='entries')
        if entries:
            for e in entries:
                entry_list.children.append(Li(
                    H2(Text(e['title'])),
                    RawText(e['text'])))
        else:
            entry_list.children.append(Li(
                Em(Text('Unbelievable. No entries here so far.'))))
        children = [entry_list]
        if session.get('logged_in'):
            form = AddEntryForm({})
            children.insert(0, form)
        super().__init__(*children)


class Field(Div):
    errors = None

    def __init__(self, input, help_text):
        input.attributes['id'] = input.attributes['name']
        super().__init__(
            Label(
                Text(input.attributes['name'].title()),
                for_=input.attributes['name']),
            input,
            P(Text(help_text), class_='help-text'),
            class_='field')
        self.input = input

    def set_errors(self, *errors):
        self.errors = list(errors)

    def render(self):
        if self.errors:
            self.children += [P(Text(e), class_='help-text error') for e in self.errors]
        return super().render()


class LoginForm(Form):

    def __init__(self, html_data):
        self.username = Field(
            Input(type='text', name='username', html_value=html_data.get('username')),
            'The username you signed up with')
        self.password = Field(
            Input(type='password', name='password', html_value=html_data.get('password')),
            'Your password')
        super().__init__(
            self.username,
            self.password,
            Input(type='submit', value='Login'),
            action=url_for('login'),
            method='POST')

    def validate(self):
        username = self.username.input.value
        password = self.password.input.value
        correct = all([
            username == current_app.config['USERNAME'],
            password == current_app.config['PASSWORD']])
        if not correct:
            self.username.set_errors('Incorrect username or password')
            self.password.set_errors('Incorrect username or password')
            return False
        return True


class LoginPage(Page):

    def __init__(self, html_data):
        self.form = LoginForm(html_data)
        super().__init__(
            H2(Text('Login')),
            self.form)
