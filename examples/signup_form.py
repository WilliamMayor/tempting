"""A page with a signup form"""
from tempting.core import Html
from tempting.tags import Head, Title, Link, Body, Header, H1, Main, Footer, Div, Ul, Li, Label
from tempting.placeholder import Placeholder
from tempting.forms import Form, Input
from tempting.errors import ValidationError

BasePage = Html.extend(
    Head(
        Title(Placeholder('title')),
        Link(
            rel='stylesheet',
            type='text/css',
            href='/static/styles.css')
    ),
    Body(
        Header(H1('Demo Page')),
        Main(Placeholder('content'), class_='content'),
        Footer('Copyright Demo Company 2017')
    )
)


class EmailInput(Input):

    def __init__(self, *args, **kwargs):
        kwargs['type'] = 'text'
        super().__init__(*args, **kwargs)

    def load(self, html_value):
        self.value = html_value

    def validate(self):
        if '@' not in self.value:
            raise ValidationError('Not a valid email address')


class Field(Div):

    def render(self, *args, **kwargs):
        input = self.find('input')
        if input and input.errors:
            kwargs['errors'] = Ul(*[Li(e) for e in input.errors], class_='errors')
        return super().render(*args, **kwargs)


SignUpPage = BasePage.extend(
    content=Form(
        Field(
            Label('Email Address', for_='email'),
            EmailInput(id='email', name='email'),
            Placeholder('errors', default=None)
        ),

        Field(
            Label('Password', for_='password'),
            Input(id='password', name='password', type='password'),
        ),

        Input(type='submit', value='Sign Up')
    ),
    title='Sign Up!'
)

SuccessPage = BasePage.extend(
    content=Div(
        Div('You signed up!'),
        Div("We'll send an email to ", Placeholder('email', default='unknown email'), ' soon!')
    ),
    title='Success!'
)

# Imagine we're in a GET method and we want to render the page with no data
# provided
page = SignUpPage()
print(page.render(_pretty=True))

# Now we're in a POST so we load in some data
form = page.find('form')[0]
form.load({'email': 'not an email', 'password': 'hunter2'})
if not form.validate():
    print(page.render(_pretty=True))
else:
    email = form.find('[name=email]')[0].value
    page = SuccessPage(email=email)
    print(page.render(_pretty=True))
